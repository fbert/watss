<html>
    <head>
	<meta name="description" content="Video tagging and results comparison">
	<meta name="keywords" content="Video, Ground truth, Keyboard, Automatic">
	<meta charset="UTF-8">
	
	<!-- CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">

        <!--<script src="js/event-handler"></script>-->
    </head>

    <body>
		<div style="margin: 15px 15px;">
			<nav class="navbar navbar-default" role="navigation">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<!--<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <a class="navbar-brand" href="#">Home</a>
				</div>-->

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse">
				  <ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
					<li><a href="gt-making/gt-making.php">GT Making</a></li>
					<li><a href="export.php">Export Results</a></li>					
					<li><a href="legend.html">Legend</a></li>
				  </ul>
				</div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>

		    <h1>WATSS: Web Annotation Tool for Surveillance Scenarios</h1>
		    <h4>This tool is designed to annotate person and group bounding boxes, visible area, head gaze, body gaze and observed points of interest (poi) on surveillance datasets.</h4> 
	     	    <h5>You may  try it on sequences acquired from the Bargello Museum, Go to <i>GT Making</i> section and enter with the user <i>Guest</i>.</h5>
		    <img src="home_page.png" alt="Screenshot of Watss">	
		</div>
 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="../js/bootstrap.min.js"></script>
	    <script src="../js/jquery.hotkeys.js"></script>
	</body>
</html>
